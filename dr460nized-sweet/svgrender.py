#!/usr/bin/python


from jinja2 import Template
from cairosvg import svg2png


# Frames per second
FPS = 10
# Duration of the animation in seconds
DUR = 3
# Width and Height of the animation in pixels
WIDTH = 256
HEIGHT = 256
# Number of frames
FRAMES = FPS * DUR


def hex_to_RGB(hex_color):
  return [int(hex_color[i:i+2], 16) for i in range(1,6,2)]


def RGB_to_hex(RGB):
  RGB = [int(x) for x in RGB]
  return "#"+"".join(["0{0:x}".format(v) if v < 16 else
            "{0:x}".format(v) for v in RGB])


def color_dict(gradient):
  return {"hex":[RGB_to_hex(RGB) for RGB in gradient],
      "r":[RGB[0] for RGB in gradient],
      "g":[RGB[1] for RGB in gradient],
      "b":[RGB[2] for RGB in gradient]}


def linear_gradient(start_hex, finish_hex="#FFFFFF", n=10):
  ''' gradient list of (n) colors between 2 hex colors.'''
  s = hex_to_RGB(start_hex)
  f = hex_to_RGB(finish_hex)
  RGB_list = [s]
  for t in range(1, n):
    curr_vector = [
      int(s[j] + (float(t)/(n-1))*(f[j]-s[j]))
      for j in range(3)
    ]
    RGB_list.append(curr_vector)

  return color_dict(RGB_list)


print(f"Rendering {FRAMES} frames ({FPS} FPS, {DUR} seconds)...")


gradient_1 = linear_gradient("#A10100", "#DA1F05", n=FRAMES)
gradient_2 = linear_gradient("#DA1F05", "#FE650D", n=FRAMES)
gradient_3 = linear_gradient("#FE650D", "#FFC11F", n=FRAMES)


with open('logo_template.svg') as f:
    template = Template(f.read())


for i in range(0, FRAMES):
    svg_code = template.render(
        width=WIDTH, height=HEIGHT,
        stop_1=gradient_1.get('hex')[i],
        stop_2=gradient_2.get('hex')[i],
        stop_3=gradient_3.get('hex')[i])
    svg2png(bytestring=svg_code.encode(), write_to=f'out/logo-{i+1}.png')


print("Done!")
