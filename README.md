# plymouth-theme-dr460nized-sweet


![Preview](https://i.imgur.com/7xOihzp.gif)


## Rendering Frames

The Python script svgrender.py will render frames for the gradient transition of the logo.

This package comes with 30 pre-rendered frames, the animation goes from frame 1-30 and back from 30-1.

The logo file (logo_template.svg) is a jinja2 template that will be filled by the script with the right colors for each frame.


Additional frames can be added to the animation by editing the svgrender.py file and the script file (MAX_FRAMES variable). The python script has `python-jinja2` and `python-cairosvg` as dependencies.

## Setup

If you are using an Arch based distro like Garuda: `paru plymouth-theme-dr460nized-sweet-git`

For any other distro: download the dr460nized-sweet folder and put it in `/usr/share/plymouth/themes`


```sudo plymouth-set-default-theme -R dr460nized-sweet```

## Copyright notice

The background image bg.png is made and owned by Eliver Lara as part of [Sweet](https://github.com/EliverLara/Sweet) Copyright (C) 2022


The dr460nized logo, which was used as a base to make the logo_template.svg file is made and owned by [SGS](https://gitlab.com/SGSm) Copyright (C) 2022

Part of the plymouth script was adapted from the original [dr460nized plymouth theme](https://gitlab.com/garuda-linux/themes-and-settings/artwork/plymouth-theme-dr460nized) by dr460nf1r3, Copyright (C) 2022



